﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class inicio : System.Web.UI.Page
{
    protected SqlConnection MiConexion = new SqlConnection(WebConfigurationManager.ConnectionStrings["CadenaConexionPERSONAT"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LlenarDatos();
        }
    }

    private void LlenarDatos()
    {
        DataTable contenido = new DataTable();
        SqlDataAdapter Consulta = new SqlDataAdapter("SELECT TOP 6 FR, idArchivoAdjunto, nombreArchivo, rutaArchivo, descArchivo, status, idTipoArchivo, publicar, (Nombre + ' ' + ApellidoPaterno + ' ' + ApellidoMaterno) AS NombreCompleto, Estado, NombreMunicipio FROM vInicioReto21Dias WHERE status='A' AND Publicar='S' ", MiConexion);
        Consulta.Fill(contenido);
        try
        {
            if (contenido.Rows.Count > 0)
            {
                rptReciente.DataSource = contenido;
                rptReciente.DataBind();
            }
        }
        catch (SystemException Error)
        {
            lblMensaje.Text = "Por el momento no se pueden mostrar los datos, intente más tarde." + Error;
            lblMensaje.Visible = true;
        }
    }
}