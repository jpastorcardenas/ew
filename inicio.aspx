﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="inicio.aspx.cs" Inherits="inicio" MasterPageFile="~/MasterPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/estilo.css" rel="stylesheet" />
     <div id="fb-root"></div>
    <script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_Body" runat="Server">
    <section>
        <div class="container-fluid">
            <div class="row sinPaddingSinMargen">
                <div class="">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <a href="/ValidaRegistro.aspx">
                                <img src="img/soporte/slider1.jpg" alt="..." />
                                <div class="carousel-caption">
                        Haz ejercicio
                                </div>
                                    </a>
                            </div>
                            <div class="item">
                                  <a href="/ValidaRegistro.aspx">
                                <img src="img/soporte/slider2.jpg" alt="..." />
                                <div class="carousel-caption">
                                    Alimentate sanamente
                                </div>
                                       </a>
                            </div>
                            <div class="item">
                                   <a href="/ValidaRegistro.aspx">
                                <img src="img/soporte/Slider3.jpg" alt="..." />
                                <div class="carousel-caption">
                                   Lleva una dieta balanceada
                                </div>
                                               </a>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="background: #fff; height: 300px; height: auto !important;">
        <div class="container">
            <div class="row" style="padding:50px 10px;">
                <div data-sr="wait 0.1s, scale up 25%, reset">
                    <div class="col-xs-12 col-sm-3">
                        <img src="img/soporte/CualEsElReto.png" alt="Cual es el reto" class="img-responsive" height="150" />
                    </div>
                    <div class="col-xs-12 col-sm-6" style="font-weight: 800;">
                       <a href="Textos/QueEs.aspx" class="btn btn-outline-inverse btn-lg">ENT&Eacute;RATE AQU&Iacute;</a>
                    </div>
                </div>
            </div>
        </div>
            <div style="height: 25px; background: #4abbd5;"></div>     
        <div class="container">
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-10" style="margin-bottom:25px;">
                    <div class="col-xs-12 col-sm-4">
                        <div data-sr="wait 0.3s, scale up 25%, reset">
                            <div class="col-xs-6" style="padding: 10px;">
                                <a href="Textos/Activate.aspx">
                                    <img src="img/soporte/LogoLograrlo.png" />
                                </a>
                            </div>
                            <div class="col-sm-6 hidden-xs" style="padding-top: 10px;">
                                <a href="Textos/Activate.aspx">
                                    <img src="img/soporte/ConoceComoLograrlo.png" />
                                </a>
                            </div>
                        </div>
                         <a href="Textos/Activate.aspx" class="btn btn-outline-inverse btn-lg">ACTIVARTE F&Iacute;SICAMENTE</a>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div data-sr="wait 0.6s, scale up 50%, reset">
                            <div class="col-xs-6" style="padding: 10px;">
                                <a href="Textos/Alimentate.aspx">
                                    <img src="img/soporte/LogoTips.png" />
                                </a>
                            </div>
                            <div class="col-sm-6 hidden-xs" style="padding-top: 10px;">
                                <a href="Textos/Alimentate.aspx">
                                    <img src="img/soporte/Tips.png" />
                                </a>
                            </div>
                        </div>
                        <a href="Textos/Alimentate.aspx" class="btn btn-outline-inverse btn-lg">ALIMENTARTE SANAMENTE</a>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div data-sr="wait 0.9s, scale up 100%, reset">
                            <div class="col-xs-6" style="padding: 10px;">
                                <a href="Textos/Interactua.aspx">
                                    <img src="img/soporte/LogoBeneficios.png" />
                                </a>
                            </div>
                            <div class="col-sm-6 hidden-xs" style="padding-top: 10px;">
                                <a href="Textos/Interactua.aspx">
                                    <img src="img/soporte/Beneficios.png" />
                                </a>
                            </div>
                        </div>
                         <a href="Textos/Interactua.aspx" class="btn btn-outline-inverse btn-lg">&nbsp;&nbsp;&nbsp;&nbsp;INTERACT&Uacute;A &nbsp;&nbsp;&nbsp;&nbsp;</a>
                    </div>
                   
                </div>
                <div class="col-xs-1"></div>
            </div>
        </div>
    </section>
    <section id="ActividadReciente" style="background: #05AC5F; height: 500px; height: auto !important;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p style="color: #fff; font-size: 35px; text-align: center;">Actividad Reciente</p>
                </div>
            </div>
            <asp:Label runat="server" ID="lblMensaje" Visible="false"></asp:Label>
            <asp:Repeater runat="server" ID="rptReciente">
                <ItemTemplate>
                    <%# (Container.ItemIndex==0) ? "<div class='row'>": "" %>
                    <div class="col-xs-12 col-sm-4">
                        <div class="thumbnail contenThumb" >

                                 <div class="contenheaderPerfil">
                                    <div class="col-xs-3">
                                        <div class="contenimgPerfil">
                                            <div id="links" class='fotosBox box'>
                                                <a href='<%: ResolveClientUrl("~/Foto/") %><%# Eval("FR")+".jpg" %>' data-gallery='<%# "#"+Eval("FR") %>'>

                                                    <img src='<%# ResolveClientUrl("~/Foto/")+Eval("FR")+".jpg" %>' title='Reto 21' class="imgPerfil">
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="contenNombrePerfil">
                                            <asp:Label runat="server"><%# Eval("NombreCompleto") %></asp:Label>
                                        </div>
                                    </div>
                                </div>

                                                            <hr />
                                <div class="contenimge">

                                    <div id="linksAjunto" class='fotosBox box'>
                                        <a href='<%: ResolveClientUrl("~/ArchivosAdjuntos/Fotos/") %><%# Eval("nombreArchivo")+".jpg" %>' data-gallery='<%# "#"+Eval("nombreArchivo") %>'>
                                            <%# (int.Parse(Eval("idTipoArchivo").ToString())==1)?"<div style='height: 150px; overflow: hidden;'>  <a href='Nota/Leer.aspx?y="+ Eval("idArchivoAdjunto")+"' target='_self' style='color: #fff; text-decoration: none;'>   <img src='"+ ResolveUrl("~/ArchivosAdjuntos/Fotos/")+Eval("nombreArchivo")  +".jpg'"+" alt='"+ Eval("descArchivo")+"' height='180' />  </a></div> ":"<div class='embed-responsive embed-responsive-16by9'  style='padding-bottom:0px; height: 150px;'>  <iframe class='youtubeIframe embed-responsive-item' src='https://www.youtube.com/embed/"+Eval("rutaArchivo").ToString().Trim().Substring(Eval("rutaArchivo").ToString().Trim().IndexOf("v=") + 2, 11)+"' frameborder='0' allowfullscreen style='height: 150px;' ></iframe></div>" %>
                                        </a>
                                    </div>
                                </div>
         
                           
                         
                            <a href='<%# ResolveUrl("Nota/Leer.aspx?y=")+ Eval("idArchivoAdjunto") %>' target='_self' style='color: #fff; text-decoration: none;'>  
                                 <div class="caption">
                                      <div style="height:110px; overflow:hidden;">
                                   <p> <%# Eval("descArchivo") %></p>
                                            <p><%# Eval("Estado") + ", " %>  <%# Eval("NombreMunicipio") %></p>
                                          </div>
                                    <br />
                                    <div class="row">
                                      <div class="col-md-12">
                                           <div>
                                                  <div class="media">
                                                      <div class="content" style=" padding: 15px;">
                                                          <div class="fb-like" data-href='<%# "http://www.reto21dias.org.mx/Nota/Leer.aspx?y=" + Eval("idArchivoAdjunto") %>' data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
                                                      </div>
                                                  </div>
                                              </div>
                                      </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div id='<%# Eval("FR") %>' class="blueimp-gallery <%--blueimp-gallery-controls--%>">
                            <div class="slides">
                            </div>
                            <h3 class="title fotoTitulo"></h3>
                            <a class="prev">‹</a> <a class="next">›</a> <a class="play-pause"></a><a class="close">×</a>
                            <ol class="indicator">
                            </ol>
                        </div>
                        <div id='<%# Eval("nombreArchivo") %>' class="blueimp-gallery <%--blueimp-gallery-controls--%>">
                            <div class="slides">
                            </div>
                            <h3 class="title fotoTitulo"></h3>
                            <a class="prev">‹</a> <a class="next">›</a> <a class="play-pause"></a><a class="close">×</a>
                            <ol class="indicator">
                            </ol>
                        </div>

                    </div>
                    <%# (Container.ItemIndex==2) ? "</div> <div class='row'>": (Container.ItemIndex==5)? "</div>": "" %>
                </ItemTemplate>
            </asp:Repeater>


        </div>
    </section>
    <section style="background: #302929;">
        <div class="container">
            <div class="row">
                <h2 class="text-center col-xs-12" style="color: #0a0809; font-weight: 800;">El Reto en las Redes</h2>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-xs-12 col-md-5" style="background: #fff;">
                    <div class="encabezadoSlider18" style="color: #fff; padding: 15px; background: #808080;">
                        <a href="https://twitter.com/GestionSocialCP" target="_blank" style="color: #fff; padding-left: 5px; font-family: lato;">TWITTER</a>
                    </div>
                    <a class="twitter-timeline" data-chrome="noheader noborders noscrollbar"
                        href="https://twitter.com/ViSCi_CENPRI" data-widget-id="711852113542811648"></a>
                    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
                </div>
                <div class="col-md-offset-1 col-xs-12 col-md-5" style="background: #fff;">
                    <div class="encabezadoSlider18" style="color: #fff; padding: 15px; background: #808080;">
                        <a href="https://www.facebook.com/pages/Vinculaci%C3%B3n-con-Sociedad-Civil-CEN-PRI/656720581018489"
                            target="_blank" style="color: #fff; padding-left: 5px; font-family: lato;">FACEBOOK</a>
                    </div>
                    <div class="mascaraFacebook18">
                        <div class="fb-like-box" data-header="false" data-height="604" data-href="https://www.facebook.com/pages/Vinculaci%C3%B3n-con-Sociedad-Civil-CEN-PRI/656720581018489"
                            data-show-border="false" data-show-faces="false" data-stream="true" data-width="344">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
